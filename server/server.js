let express = require('express');
let path = require('path');
let app = express();
let port = 4200;
app.use(express.static('./run'));
app.get("*", (req, res, next) => {
    res.sendFile(path.resolve('./run/index.html'));
})
app.listen(port, () => {
    console.info("Application'server connect successfully...!");
    console.info("Across port:", port);
})