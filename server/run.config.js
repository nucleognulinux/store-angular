module.exports = {
  apps: [
    {
      name: "Frontend",
      script: "server.js",
      instances: 2,
      autorestart: true,
      watch: true,
      max_memory_restart: "1G"
    }
  ]
};