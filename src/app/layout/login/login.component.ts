import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthorizationService } from '@Services/authorization.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  private get loginValue() { return this.loginForm.value }

  public loginForm: FormGroup;

  constructor(private fb: FormBuilder, private authorizationService: AuthorizationService, private router: Router) {
    this.loginForm = fb.group({
      user: [],
      password: [],
      remember: []
    })
  }

  public login(): void {
    this.authorizationService.setAuthorization(this.loginValue, () => {
      this.router.navigateByUrl("/app")
    });

  }

}
