import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '@Services/authorization.service';
import { Router } from '@angular/router';
import { ProductsService } from '@Services/products.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  constructor(private authorizationService: AuthorizationService, private router: Router) { }

  ngOnInit(){
    
  }
  public logout(): void {
    this.authorizationService.logout(() => {
      this.router.navigateByUrl("/");
    });
  }
  
}
