import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductsService } from '@Services/products.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {
  public listProducts: any[] = [];
  public isVisible: boolean = false;
  public productForm: FormGroup;
  private get productFormValue(): any { return this.productForm.value }
  private unsubscribe$: Subject<void> = new Subject<void>();

  constructor(private productsService: ProductsService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.getProducts();
    this.productFormInit();
    interval(10).pipe(takeUntil(this.unsubscribe$)).subscribe(time => {
      console.log("hola sam", time)
    })
  }
  ngOnDestroy(): void {
    console.log("hola cechus")
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public productFormInit() {
    this.productForm = this.fb.group({
      id: [],
      name: []
    })
  }


  public getProducts() {
    this.productsService.getProducts().pipe(takeUntil(this.unsubscribe$)).subscribe(res => {
      this.listProducts = res;
    })
  }

  /**
   * 
   * @param product this param is the object that it arrives from form
   * @param pos this is position the array list
   */
  public editProduct(product: any, pos: number) {
    if (this.listProducts[pos].price) {
      this.productForm.patchValue(
        {
          id: pos,
          name: product.name
        }
      )
      this.isVisible = true;
    }
  }
  /**
   * this method is to change the product properties
   */
  public changeProduct() {
    const product: any = this.productFormValue;
    this.listProducts[product.id].name = product.name;
    this.listProducts[product.id].price = 0;
    this.isVisible = false;
  }
}
