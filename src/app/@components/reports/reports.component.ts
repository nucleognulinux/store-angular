import { Component, OnInit } from '@angular/core';
import { ProductsService } from '@Services/products.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  listProducts: any[] = [];

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  public getProducts() {
    this.productsService.getProducts().subscribe(res => {
      console.log(res)
      this.listProducts = res;
      console.log(this.listProducts)
    })
  }

}
