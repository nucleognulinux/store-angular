import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private html: HttpClient) { }

  public getProducts(): Observable<any[]> {
    return this.html.get<any[]>("http://localhost:3000/products");
  }
}
