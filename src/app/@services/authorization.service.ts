import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  public get session() { return localStorage.getItem("SESSION"); }

  constructor() { }

  private setSession(user: string): void {
    localStorage.setItem("SESSION", user);
  }

  public setAuthorization(login: any, redirect: Function): void {
    this.setSession(login.user);
    redirect();
  }

  public logout(logoutRedirect: Function): void {
    localStorage.removeItem("SESSION");
    logoutRedirect();
  }

}
