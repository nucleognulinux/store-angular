
import { LoginComponent } from './layout/login/login.component';
import { MainComponent } from './layout/main/main.component';
import { RoleGuard } from './@guards/role.guard';
import { LoginGuard } from './@guards/login.guard';


import { ProductsComponent } from '@Components/products/products.component';
import { ReportsComponent } from '@Components/reports/reports.component';
export const router = [
    {
        path: "",
        component: LoginComponent,
        canActivate: [LoginGuard]
    },
    {
        path: "app",
        component: MainComponent,
        canActivate: [RoleGuard],
        children: [
            {
                path: 'products',
                component: ProductsComponent
            },
            {
                path: 'report',
                component: ReportsComponent
            }
        ]
    },
    {
        path: '**',
        redirectTo: '/app',
        pathMatch: 'full'
    },
]