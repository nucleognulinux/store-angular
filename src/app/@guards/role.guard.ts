import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizationService } from '@Services/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(private router: Router, private authorizationService: AuthorizationService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const redirectUrl = next.url;
    console.log(this.authorizationService.session)
    if (!this.authorizationService.session) {
      this.router.navigateByUrl(
        this.router.createUrlTree(
          ['/'], {
          queryParams: {
            redirectUrl
          }
        }
        ));
      return false;
    }
    return true;
  }

}
